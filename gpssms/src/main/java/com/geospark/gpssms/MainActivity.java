package com.geospark.gpssms;

import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private LocationListener _location_listener = null;
    private boolean _sending_location = false;
    private Handler _countdown_handler = null;
    private int minutecount = 0;

    // option to scale back when in development mode
    private int milisecondsdelay = 60 * 1000;
    //private int milisecondsdelay = 5 * 1000;
    private double sensitivitydistance = 50.01;
    //private double sensitivitydistance = 0.01;
    private String locationprovider = LocationManager.GPS_PROVIDER;
    //private String locationprovider = LocationManager.NETWORK_PROVIDER;

    // phone numbers are in strings and can have name of person at start which is stripped out
    // 07916090736 Julian
    // 07527268165  Becka
    // 07732390829  Tom Caver

    private Location firstlocation = null;
    private Location recentlocation = null;
    private WakeLock wakeLock = null;

    private Runnable _countdown = new Runnable() {
        @Override
        public void run() {
            Log.d("DD", "got into run");
            EditText etinterval = (EditText)findViewById(R.id.txt_interval);
            TextView tvstatus = (TextView)findViewById(R.id.txt_status);
            int tinterval = Integer.parseInt(etinterval.getText().toString());
            if ((firstlocation == null) || (minutecount >= tinterval)) {
                String sstatus = "Reading GPS";
                tvstatus.setText(sstatus.toCharArray(), 0, sstatus.length());
                LocationManager location_manager = (LocationManager)MainActivity.this.getSystemService(MainActivity.LOCATION_SERVICE);
                location_manager.requestLocationUpdates(locationprovider, 0, 0, _location_listener);
            } else {
                String sminutecount = String.format("Minutes %d", minutecount);
                tvstatus.setText(sminutecount.toCharArray(), 0, sminutecount.length());
                Log.d("STAT", sminutecount);
                minutecount++;
                _countdown_handler.postDelayed(this, milisecondsdelay);  // wait one minute
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("LOCSTAT", "onResume called");

        Button btn_start_stop = (Button)findViewById(R.id.btn_start_stop);
        btn_start_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DD", "button press");
                Button btn = (Button)v;

                // start case
                if (!_sending_location) {
                    btn.setText(R.string.stop);
                    minutecount = 0;
                    _countdown_handler.post(_countdown);
                    _sending_location = true;

                    PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
                    wakeLock.acquire();
                    Log.d("LOCK", (wakeLock.isHeld() ? "wakelockheld" : "wakelockfree"));
                // stop case
                } else {
                    stopeverything(true);
                }
            }
        });

        _location_listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                LocationManager location_manager = (LocationManager)MainActivity.this.getSystemService(MainActivity.LOCATION_SERVICE);
                location_manager.removeUpdates(this);
                Log.d("LOCLOC", location.getProvider().toString());
                EditText etcount = (EditText)findViewById(R.id.txt_count);
                int countremaining = Integer.parseInt(etcount.getText().toString());
                if (firstlocation == null) {
                    firstlocation = location;
                    recentlocation = location;
                    Log.d("LOCLOC", "first location set");
                    String smess = String.format("JGT[%s.%d] starting at http://maps.google.com/?q=@%.4f,%.4f alt:%.0fm", location.getProvider().toString(), countremaining, location.getLatitude(), location.getLongitude(), location.getAltitude());
                    send_message(smess);  // comment out if you don't want a first message
                } else {
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();
                    double alt = location.getAltitude();
                    double dst = firstlocation.distanceTo(location);
                    double bearing = firstlocation.bearingTo(location);
                    double lastdst = recentlocation.distanceTo(location);
                    if (lastdst >= sensitivitydistance) {
                        Log.d("LOCDST", String.format("%f", dst));
                        String smess = String.format("JGT[%s.%d] at http://maps.google.com/?q=@%.4f,%.4f alt:%.0fm gone %.0fm in %.0fdeg", location.getProvider().toString(), countremaining, location.getLatitude(), location.getLongitude(), location.getAltitude(), dst, bearing);
                        send_message(smess);
                        recentlocation = location;
                    } else {
                        Log.d("LOCDST", String.format("skipping message as last dist %f", lastdst));
                    }
                    if (!_sending_location) {
                        return;
                    }
                }

                countremaining--;
                if (countremaining < 0) {
                    stopeverything(true);
                    return;
                }
                minutecount = 0;
                String scountremaining = String.format("%d", countremaining);
                etcount.setText(scountremaining.toCharArray(), 0, scountremaining.length());
                _countdown_handler.post(_countdown);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("LOCSTAT", String.format("%s %d", provider, status));
            }

            @Override
            public void onProviderEnabled(String provider) {}

            @Override
            public void onProviderDisabled(String provider) {}
        };

        // We use a handler to start the location gathering. When start is pressed, a Java runnable,
        // which enables GPS locations, is added to the handler's message queue.
        _countdown_handler = new Handler();

        // auto-complete phone numbers to make it easier
        AutoCompleteTextView textView = (AutoCompleteTextView)findViewById(R.id.txt_phone_number);
        String[] phone_numbers = getResources().getStringArray(R.array.phone_numbers_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, phone_numbers);
        textView.setAdapter(adapter);
    }




    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LOCSTAT", "onStop called");
        //stopeverything(false); // able to resume
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("LOCSTAT", "onResume called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void stopeverything(boolean breset) {
        Button btn_start_stop = (Button)findViewById(R.id.btn_start_stop);
        LocationManager location_manager = (LocationManager)this.getSystemService(MainActivity.LOCATION_SERVICE);
        location_manager.removeUpdates(_location_listener);
        if (_countdown_handler != null && _countdown != null) {
            _countdown_handler.removeCallbacks(_countdown);
        }
        _countdown_handler.removeCallbacks(_countdown);
        wakeLock.release();
        Log.d("LOCK", (wakeLock.isHeld() ? "wakelockheld" : "wakelockfree"));
        wakeLock = null;
        btn_start_stop.setText(R.string.start);
        TextView tvstatus = (TextView)findViewById(R.id.txt_status);
        String sstatus = (breset ? "Stopreset" : "Stopped");
        tvstatus.setText(sstatus.toCharArray(), 0, sstatus.length());
        _sending_location = false;
        if (breset)
            firstlocation = null;
    }

    void send_message(String smess) {
        try {
            SmsManager mgr = SmsManager.getDefault();
            Log.d("LOCATION", smess);
            EditText etphonenumber = (EditText)findViewById(R.id.txt_phone_number);
            String sphonenumber = etphonenumber.getText().toString();
            String cleanphonenumber = sphonenumber.replaceAll("[^0-9]","");
            Log.d("PHONENUMBERfield", "::"+sphonenumber+":->:"+cleanphonenumber+"::");
            if (cleanphonenumber.length() >=6)
                mgr.sendTextMessage(cleanphonenumber, null, smess, null, null);
            else
                Log.d("PHONENUMBERfield", "no message sent: phone number too short");
        } catch (IllegalArgumentException e) {
            Toast.makeText(MainActivity.this, R.string.cannot_send, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}

